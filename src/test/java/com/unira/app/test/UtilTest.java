package com.unira.app.test;

import org.junit.Test;
import static org.junit.Assert.*;

import com.unira.app.Util;

public class UtilTest {

	@Test
	public void testReadAndUpdateJobFile() {
		String expJobId = "0059";
		String actJobId = Util.readAndUpdateJobFile("D:/Sumeendar_ISO/jobFile.txt");
		assertEquals(expJobId, actJobId);
	}
	
	@Test
	public void testReadAndUpdateJobFile1() {
 		String expJobId = "0003";
		String actJobId = Util.readAndUpdateJobFile("D:/Sumeendar_ISO/jobFile1.txt");
	 	assertEquals(expJobId, actJobId);
	}

}
