package com.unira.app.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

import com.unira.app.FileProcessor;
import com.unira.app.Validator;

public class FileProcessorTest {


	FileProcessor fileProcess;
	Method m1;
	
 	private static final Logger logger = LogManager.getLogger(FileProcessorTest.class);
	
 	@Test
	public void testTlValidation() {
 		fileProcess = new FileProcessor();
 		Validator validator = new Validator();
 		fileProcess.setValidator(validator);
		String tlField = "TL12350112163";
		callTLValidationMethod();
		boolean result = generalCode(tlField);
		assertFalse(result);
 	}	
		
 	@Test
	public void testTlValidation1() {
 		fileProcess = new FileProcessor();
 		Validator validator = new Validator();
 		fileProcess.setValidator(validator);
		String tlField = "TL03711109163101 100            CC";
		callTLValidationMethod();
		boolean result = generalCode(tlField);
		assertTrue(result);
 	}	
 	
 	@Test
 	public void testUpdateTlRecord(){
 		fileProcess = new FileProcessor();
 		String exptedTL = "TL03711109163101 100            CC000001250";
 		String tlField = "TL03711109163101 100            CC";
 		int totalRecordCount = 1250;
 		callUpdateTlRecordMethod();
 		String updtedTL = generalCode(tlField, totalRecordCount);
 		assertEquals(null, exptedTL, updtedTL);
 	}
 	
 	@Test
 	public void testProcessRecords(){
 		fileProcess = new FileProcessor();
 		fileProcess.setSignConversionMap(getConversionMap());
 		callProcessRecordsMethod();
 		int expCnt = 13;
 		int scCount = generalCode(getSCRecords());
 		assertEquals(expCnt, scCount);
 	}
 	
 	@Test
 	public void testProcessRecords1(){
 		List<String> scRecords = new ArrayList<String>();
 		scRecords.add("SC010371963101000000220J00000010D");
 		scRecords.add("SC020371963101000000152R000000025");
 		fileProcess = new FileProcessor();
 		fileProcess.setSignConversionMap(getConversionMap());
 		callProcessRecordsMethod();
 		int expCnt = 129;
 		int scCount = generalCode(scRecords);
 		assertEquals(expCnt, scCount);
 	}
 	
 	@Test
 	public void testGetDollarAmountCount(){
 		fileProcess = new FileProcessor();
 		fileProcess.setSignConversionMap(getConversionMap());
 		String screcord = "SC010371963101000000220J00000010D";
 		int startLen = 14;
 		int endLen = 24;
 		callGetDollarAmountCountMethod();
 		String expAmtCnt = "-0000002201";
 		String scAmtCnt = generalCode(screcord, startLen, endLen);
 		assertEquals(expAmtCnt, scAmtCnt);
 	}
 	
 	@Test
 	public void testGetDollarAmountCount1(){
 		fileProcess = new FileProcessor();
 		fileProcess.setSignConversionMap(getConversionMap());
 		String screcord1 = "SC010371963101000000220{00000010D";
 		int startLen1 = 14;
 		int endLen1 = 24;
 		callGetDollarAmountCountMethod();
 		String expAmtCnt = "0000002200";
 		String scAmtCnt = generalCode(screcord1, startLen1, endLen1);
 		assertEquals(expAmtCnt, scAmtCnt);
 	}
 	
 	@Test
 	public void testGetDollarAmountCount2(){
 		fileProcess = new FileProcessor();
 		fileProcess.setSignConversionMap(getConversionMap());
 		String screcord2 = "SC010371963101000000220{00000010D";
 		int startLen2 = 24;
 		int endLen2 = 33;
 		callGetDollarAmountCountMethod();
 		String expAmtCnt = "000000104";
 		String scAmtCnt = generalCode(screcord2, startLen2, endLen2);
 		assertEquals(expAmtCnt, scAmtCnt);
 	}
 	
 	@Test
 	public void testGetDollarAmountCount3(){
 		fileProcess = new FileProcessor();
 		fileProcess.setSignConversionMap(getConversionMap());
 		String screcord3 = "SC010371963101000000220{00000010}";
 		int startLen3 = 24;
 		int endLen3 = 33;
 		callGetDollarAmountCountMethod();
 		String expAmtCnt = "-000000100";
 		String scAmtCnt = generalCode(screcord3, startLen3, endLen3);
 		assertEquals(expAmtCnt, scAmtCnt);
 	}
 	
 	@Test
 	public void testReadSCRecordsFromFile(){
 		String scFile = "D:/Sumeendar_ISO/scRecords.txt";
 		fileProcess = new FileProcessor();
 		callReadScRecordsFromFileMethod();
 		List<String> scRecords = generalCode1(scFile);
 		@SuppressWarnings("rawtypes")
		Iterator itr = scRecords.iterator();
 		String row;
 		while (itr.hasNext()){
 			row = (String) itr.next();
 			if (("").equals(row)){
 				return;
 			}
 			logger.debug(row);
 		}
 	}
 	
 	@Test
 	public void testReadTLRecordFromFile(){
 		String tlFile = "D:/Sumeendar_ISO/tlRecord.txt";
 		fileProcess = new FileProcessor();
 		callReadTLRecordFromFileMethod();
 		String tlRecord = generalCode2(tlFile);
 		logger.debug(tlRecord);
 	}
 	
 	@Test
 	public void testCreateNewTL(){
 		String scFile = "D:/Sumeendar_ISO/scRecords.txt";
 		String tlField = "TL03711109163101 100            CC";
 		fileProcess = new FileProcessor();
 		fileProcess.setSignConversionMap(getConversionMap());
 		fileProcess.setScRecordFilePath(scFile);
 		fileProcess.setTlField(tlField);
 		callCreateNewTLMethod();
 		generalCode();
 	}
 	
 	@Test
 	public void testAccessTL(){
 		String unzipFolder = "D:";
 		String archiveFileBaseName = "Sumeendar_ISO";
 		String tlRecordFileName = "tlRecord.txt";
 		fileProcess = new FileProcessor();
 		fileProcess.setUnzipFolder(unzipFolder);
 		fileProcess.setArchiveFileBaseName(archiveFileBaseName);
 		fileProcess.setTlRecordFileName(tlRecordFileName);
 		callAccessTLMethod();
 		generalCode();
 	}
 	
 	
 	@Test
	public void callTLValidationMethod(){
		try {
			m1 = FileProcessor.class.getDeclaredMethod("tlValidation", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
 	
	@Test
	public void callUpdateTlRecordMethod(){
		try {
			m1 = FileProcessor.class.getDeclaredMethod("updateTlRecord", String.class, int.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	@Test
	public void callProcessRecordsMethod(){
		try {
			m1 = FileProcessor.class.getDeclaredMethod("processRecords", List.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	@Test
	public void callGetDollarAmountCountMethod(){
		try {
			m1 = FileProcessor.class.getDeclaredMethod("getDollarAmountCount", String.class, int.class, int.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	@Test
	public void callReadScRecordsFromFileMethod(){
		try {
			m1 = FileProcessor.class.getDeclaredMethod("readScRecordsFromFile", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	@Test
	public void callReadTLRecordFromFileMethod(){
		try {
			m1 = FileProcessor.class.getDeclaredMethod("readTlFieldFromFile", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	@Test
	public void callCreateNewTLMethod(){
		try {
			m1 = FileProcessor.class.getDeclaredMethod("createNewTL");
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	@Test
	public void callAccessTLMethod(){
		try {
			m1 = FileProcessor.class.getDeclaredMethod("tlAccess");
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	public void generalCode(){
		m1.setAccessible(true);
		try {
			m1.invoke(fileProcess);
		} catch (IllegalAccessException e) {
			logger.log(null, "Illegal Exception error", e);	
		} catch (IllegalArgumentException e) {
			logger.log(null, "Argument Exception error", e);	
		} catch (InvocationTargetException e) {
			logger.log(null, "Invocation Target Exception error", e);	
		}
	}
	
 	public boolean generalCode(String tlField){
 		boolean isVal = false;
 		m1.setAccessible(true);
		try {
			isVal = (Boolean) m1.invoke(fileProcess, tlField);
		} catch (IllegalAccessException e) {
			logger.log(null, "Illegal Exception error", e);	
		} catch (IllegalArgumentException e) {
			logger.log(null, "Argument Exception error", e);	
		} catch (InvocationTargetException e) {
			logger.log(null, "Invocation Target Exception error", e);	
		}
		return isVal; 
	}
 	
 	public String generalCode(String tlField, int recCount){
 		String uptedTL = null;
 		m1.setAccessible(true);
		try {
			uptedTL = (String) m1.invoke(fileProcess, tlField, recCount);
		} catch (IllegalAccessException e) {
			logger.log(null, "Illegal Exception error", e);	
		} catch (IllegalArgumentException e) {
			logger.log(null, "Argument Exception error", e);	
		} catch (InvocationTargetException e) {
			logger.log(null, "Invocation Target Exception error", e);	
		}
		return uptedTL; 
	}
 	
 	public int generalCode(List<String> scRecords){
 		Integer scCount = 0;
 		m1.setAccessible(true);
		try {
			scCount = (Integer) m1.invoke(fileProcess, scRecords);
		} catch (IllegalAccessException e) {
			logger.log(null, "Illegal Exception error", e);	
		} catch (IllegalArgumentException e) {
			logger.log(null, "Argument Exception error", e);	
		} catch (InvocationTargetException e) {
			logger.log(null, "Invocation Target Exception error", e);	
		}
		return scCount; 
	}
 	
 	public String generalCode(String screcord, int startLen, int endLen){
 		String strAmtCnt = null;
 		m1.setAccessible(true);
		try {
			strAmtCnt = (String) m1.invoke(fileProcess, screcord, startLen, endLen);
		} catch (IllegalAccessException e) {
			logger.log(null, "Illegal Exception error", e);	
		} catch (IllegalArgumentException e) {
			logger.log(null, "Argument Exception error", e);	
		} catch (InvocationTargetException e) {
			logger.log(null, "Invocation Target Exception error", e);	
		}
		return strAmtCnt; 
	}
 	
 	@SuppressWarnings("unchecked")
	public List<String> generalCode1(String tlField){
 		List<String> scRecords = null;
 		m1.setAccessible(true);
		try {
			scRecords = (List<String>) m1.invoke(fileProcess, tlField);
		} catch (IllegalAccessException e) {
			logger.log(null, "Illegal Exception error", e);	
		} catch (IllegalArgumentException e) {
			logger.log(null, "Argument Exception error", e);	
		} catch (InvocationTargetException e) {
			logger.log(null, "Invocation Target Exception error", e);	
		}
		return scRecords; 
	}

	public String generalCode2(String tlField){
 		String tlRecord = null;
 		m1.setAccessible(true);
		try {
			tlRecord = (String) m1.invoke(fileProcess, tlField);
		} catch (IllegalAccessException e) {
			logger.log(null, "Illegal Exception error", e);	
		} catch (IllegalArgumentException e) {
			logger.log(null, "Argument Exception error", e);	
		} catch (InvocationTargetException e) {
			logger.log(null, "Invocation Target Exception error", e);	
		}
		return tlRecord; 
	}

 	public Map<String, Integer> getConversionMap(){
 		Map<String, Integer> signConversionMap = new HashMap<String, Integer>();
		signConversionMap.put("A", 1);
		signConversionMap.put("B", 2);
		signConversionMap.put("C", 3);
		signConversionMap.put("D", 4);
		signConversionMap.put("E", 5);
		signConversionMap.put("F", 6);
		signConversionMap.put("G", 7);
		signConversionMap.put("H", 8);
		signConversionMap.put("I", 9);
		signConversionMap.put("J", -1);
		signConversionMap.put("K", -2);
		signConversionMap.put("L", -3);
		signConversionMap.put("M", -4);
		signConversionMap.put("N", -5);
		signConversionMap.put("O", -6);
		signConversionMap.put("P", -7);
		signConversionMap.put("Q", -8);
		signConversionMap.put("R", -9);
		return signConversionMap;
 	}
 	
 	public List<String> getSCRecords(){
 		List<String> scRecords = new ArrayList<String>();
 		scRecords.add("SC010371963101000000220J00000000D");
 		scRecords.add("SC020371963101000000152R00000000I");
		return scRecords;
 	}
}
