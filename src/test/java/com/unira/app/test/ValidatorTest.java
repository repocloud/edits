package com.unira.app.test;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.unira.app.Status;
import com.unira.app.Validator;

public class ValidatorTest {

	Validator validator;
	Method m1;
	
 	private static final Logger logger = LogManager.getLogger(ValidatorTest.class);
	
	@Test
	public void testTLId() {
		validator = new Validator();
		Status status1 = Status.SUCCESS;
		String tlField = "TL123";
		callTLIdMethod();
		generalCode(tlField, status1);
	
	}

	@Test
	public void testTLId1() {
		validator = new Validator();
		Status status2 = Status.INITIAL_TL_CHECK_FAILED;
		String tlField1 = "T1123";
		callTLIdMethod();
		generalCode(tlField1, status2);
	}
	
	@Test
	public void testTLGroup() {
		validator = new Validator();
		Status status3 = Status.SUCCESS;
		String tlField2 = "TL1234";
		callTLGroupMethod();
		generalCode(tlField2, status3);
	}
	
	@Test
	public void testTLGroup1() {
		validator = new Validator();
		Status status4 = Status.INITIAL_TL_CHECK_FAILED;
		String tlField3 = "TL123A";
		callTLGroupMethod();
		generalCode(tlField3, status4);
	}
	
	@Test
	public void testTLPlan() {
		validator = new Validator();
		Status status5 = Status.SUCCESS;
		String tlField4 = "TL123401";
		callTLPlanMethod();
		generalCode(tlField4, status5);
	}
	
	@Test
	public void testTLPlan1() {
		validator = new Validator();
		Status status6 = Status.INITIAL_TL_CHECK_FAILED;
		String tlField5 = "TL123405";
		callTLPlanMethod();
		generalCode(tlField5, status6);
	}
	
	@Test
	public void testTLAcctDate() {
		validator = new Validator();
		Status status7 = Status.SUCCESS;
		String tlField6 = "TL1235011216";
		callTLAcctDateMethod();
		generalCode(tlField6, status7);
	}
	
	@Test
	public void testTLAcctMonth() {
		validator = new Validator();
		Status status8 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlField7 = "TL1235011316";
		callTLAcctDateMethod();
		generalCode(tlField7, status8);
	}
	
	@Test
	public void testTLAcctYear() {
		validator = new Validator();
		Status status9 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlField8 = "TL123501121A";
		callTLAcctDateMethod();
		generalCode(tlField8, status9);
	}
	
	@Test
	public void testTLStatType() {
		validator = new Validator();
		Status status10 = Status.SUCCESS;
		String tlField9 = "TL12350112163";
		callTLStatTypeMethod();
		generalCode(tlField9, status10);
	}
	
	@Test
	public void testTLStatType1() {
		validator = new Validator();
		Status status11 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlField10 = "TL12350112164";
		callTLStatTypeMethod();
		generalCode(tlField10, status11);
	}
	
	@Test
	public void testTLSubmissionType() {
		validator = new Validator();
		Status status12 = Status.SUCCESS;
		String tlSubmType = "6";
		callTLSubmissionTypeMethod();
		generalCode(tlSubmType, status12);
	}
	
	@Test
	public void testTLSubmissionType1() {
		validator = new Validator();
		Status status13 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlSubmType = "4";
		callTLSubmissionTypeMethod();
		generalCode(tlSubmType, status13);
	}
	
	@Test
	public void testTLSubmType3And5() {
		validator = new Validator();
		Status status14 = Status.SUCCESS;
		String tlReSubmType = "6";
		String tlReSubmCnt = "01";
		callTLSubmType3And5Method();
		generalCode(tlReSubmType, tlReSubmCnt, status14);
	}
	
	@Test
	public void testTLSubmType3And5_1() {
		validator = new Validator();
		Status status15 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlReSubmType1 = "1";
		String tlReSubmCnt1 = "00";
		callTLSubmType3And5Method();
		generalCode(tlReSubmType1, tlReSubmCnt1, status15);
	}
	
	@Test
	public void testTLSubmType3And5_2() {
		validator = new Validator();
		Status status16 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlReSubmType2 = "5";
		String tlReSubmCnt2 = "02";
		callTLSubmType3And5Method();
		generalCode(tlReSubmType2, tlReSubmCnt2, status16);
	}
	
	@Test
	public void testTLSubmTypeOth3And5() {
		validator = new Validator();
		Status status17 = Status.SUCCESS;
		String tlReSubmType3 = " ";
		String tlReSubmCnt3 = "  ";
		callTLOthSubmType3And5Method();
		generalCode(tlReSubmType3, tlReSubmCnt3, status17);
	}
	
	@Test
	public void testTLSubmTypeOth3And5_1() {
		validator = new Validator();
		Status status18 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlReSubmType4 = "2";
		String tlReSubmCnt4 = "  ";
		callTLOthSubmType3And5Method();
		generalCode(tlReSubmType4, tlReSubmCnt4, status18);
	}
	
	@Test
	public void testTLSubmTypeOth3And5_2() {
		validator = new Validator();
		Status status19 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlReSubmType5 = " ";
		String tlReSubmCnt5 = "02";
		callTLOthSubmType3And5Method();
		generalCode(tlReSubmType5, tlReSubmCnt5, status19);
	}
	
	@Test
	public void testTLSubmissionCount() {
		validator = new Validator();
		Status status20 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlField11 = "TL12350112163A00"; 
		callTLSubmissionCountMethod();
		generalCode(tlField11, status20);
	}
	
	@Test
	public void testTLSubmissionCount1() {
		validator = new Validator();
		Status status21 = Status.SUCCESS;
		String tlField12 = "TL12350112163A01"; 
		callTLSubmissionCountMethod();
		generalCode(tlField12, status21);
	}
	
	@Test
	public void testTLFinalParIndicator() {
		validator = new Validator();
		Status status22 = Status.SUCCESS;
		validator.setTlSubmissionType("2");
		String tlField13 = "TL12350112163A01 "; 
		callTLPartialIndicatorMethod();
		generalCode(tlField13, status22);
	}
	
	@Test
	public void testTLFinalParIndicator1() {
		validator = new Validator();
		Status status23 = Status.SUCCESS;
		validator.setTlSubmissionType("2");
		String tlField14 = "TL12350112163A01X"; 
		callTLPartialIndicatorMethod();
		generalCode(tlField14, status23);
	}
	
	@Test
	public void testTLFinalParIndicator2() {
		validator = new Validator();
		Status status24 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		validator.setTlSubmissionType("2");
		String tlField15 = "TL12350112163A010"; 
		callTLPartialIndicatorMethod();
		generalCode(tlField15, status24);
	}
	
	
	@Test
	public void testTLFinalParIndicator3() {
		validator = new Validator();
		Status status25 = Status.SUCCESS;
		validator.setTlSubmissionType("1");
		String tlField16 = "TL12350112163A01 "; 
		callTLPartialIndicatorMethod();
		generalCode(tlField16, status25);
	}
	
	@Test
	public void testTLFinalParIndicator4() {
		validator = new Validator();
		Status status26 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		validator.setTlSubmissionType("1");
		String tlField17 = "TL12350112163A01X"; 
		callTLPartialIndicatorMethod();
		generalCode(tlField17, status26);
	}
	
	@Test
	public void testTLSarMedia(){
		validator = new Validator();
		Status status27 = Status.SUCCESS;
		String tlField18 = "TL12350112163A01X               CN"; 
		callTLSARMediaMethod();
		generalCode(tlField18, status27);
	}
	
	@Test
	public void testTLSarMedia1(){
		validator = new Validator();
		Status status28 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlField19 = "TL12350112163A01X               CA"; 
		callTLSARMediaMethod();
		generalCode(tlField19, status28);
	}
	
	@Test
	public void testTLSarMedia2(){
		validator = new Validator();
		Status status29 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlField20 = "TL12350112163A01X                 "; 
		callTLSARMediaMethod();
		generalCode(tlField20, status29);
	}
	
	@Test
	public void testValidateTL(){
		validator = new Validator();
		Status status30 = Status.INITIAL_TL_CHECK_FAILED;
		String tlField21 = "TL123408";
		callValidateTLMethod();
		generalCode(tlField21, status30);		
	}
	
	@Test
	public void testValidateTL1(){
		validator = new Validator();
		Status status31 = Status.SUBSEQUENT_TL_CHECK_FAILED;
		String tlField22 = "TL123401";
		callValidateTLMethod();
		generalCode(tlField22, status31);		
	}
	
	@Test
	public void callTLIdMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLId", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	

	@Test
	public void callTLGroupMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLGroup", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	@Test
	public void callTLPlanMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLPlan", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	@Test
	public void callTLAcctDateMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLAcctDate", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	@Test
	public void callTLStatTypeMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLStatType", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}	
	}
	
	@Test
	public void callTLSubmissionTypeMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLSubmissionType", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	@Test
	public void callTLSubmType3And5Method(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLSubmType3And5", String.class, String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	@Test
	public void callTLOthSubmType3And5Method(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLSubmTypeOth3And5", String.class, String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	@Test
	public void callTLSubmissionCountMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLSubmissionCount", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	@Test
	public void callTLPartialIndicatorMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLFinalParIndicator", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	@Test
	public void callTLSARMediaMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("TLSarMedia", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	@Test
	public void callValidateTLMethod(){
		try {
			m1 = Validator.class.getDeclaredMethod("validateTL", String.class);
		} catch (Exception e) {
			logger.log(null, "Method calling error", e);	
		}
	}
	
	
	public void generalCode(String tlField, Status status1){
		m1.setAccessible(true);
		Status status;
		try {
			status = (Status) m1.invoke(validator, tlField);
			assertEquals(status1, status);
		} catch (IllegalAccessException e) {
			logger.log(null, "Illegal Exception error", e);	
		} catch (IllegalArgumentException e) {
			logger.log(null, "Argument Exception error", e);	
		} catch (InvocationTargetException e) {
			logger.log(null, "Invocation Target Exception error", e);	
		} 
	}
	

	public void generalCode(String tlField1, String tlField2, Status status1){
		m1.setAccessible(true);
		try {
			Status status = (Status) m1.invoke(validator, tlField1, tlField2);
			assertEquals(status1, status);
		} catch (IllegalAccessException e1) {
			logger.log(null, "Illegal Exception error", e1);	
		} catch (IllegalArgumentException e1) {
			logger.log(null, "Argument Exception error", e1);	
		} catch (InvocationTargetException e1) {
			logger.log(null, "Invocation Target Exception error", e1);	
		} 
	}
}
