package com.unira.app;

/**
 * 
 * Common constants
 * 
 */
public final class Constants {

	public static final String CONFIG_FILE_PATH = "/isoapps/t/unix/apps/88/SFAA/ceptest/unira/config/sweep.cfg";
	public static final String ENCODING_UTF_8 = "UTF-8";
	public static final String BASE_DIRECTORY = "baseDir";
	public static final String IS_PARALLEL_SWEEP = "parallelSweep";
	public static final String THREAD_POOL_SIZE = "thread_pool_size";
	public static final String TIMEOUT = "timeout";
	public static final String TL_EXTRACT_FILENAME = "TL.txt";
	public static final String SC_EXTRACT_FILENAME = "SC.txt";
	public static final String EMAIL_LIST_FILE_NAME = "email.txt";
	public static final String JOB_FILE_NAME = "job.txt";
	public static final String LEFT_PARENTHESIS = "{";
	public static final String RIGHT_PARENTHESIS = "}";
	
	private Constants(){
		
	}
}
