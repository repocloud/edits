package com.unira.app;

import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.commons.configuration.Configuration;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * Wrapper class to access the spring beans
 * 
 */
public class AppContext implements ApplicationContextAware {
	ApplicationContext context;

	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		this.context = context;

	}

	public SweepJob geSweepJob() {
		return (SweepJob) context.getBean(Beans.SWEEP_JOB);
	}

	public String getBaseFolder() {
		return (String) context.getBean(Beans.BASE_FOLDER);
	}

	public String getInputFolder() {
		return (String) context.getBean(Beans.INPUT_FOLDER);
	}

	public ScriptExecutor getScriptExecutor() {
		return context.getBean(ScriptExecutor.class);
	}

	public Validator getValidator() {
		return context.getBean(Validator.class);
	}

	public FileProcessor getFileProcessor() {
		return context.getBean(FileProcessor.class);
	}

	public ExecutorService getExecutorService() {
		return context.getBean(ExecutorService.class);
	}

	public int getTimeoutForSweepProcess() {
		Configuration configuration = context.getBean(Configuration.class);
		return configuration.getInt(Constants.TIMEOUT);
	}
	
	public SendEmail getEmailService() {
		return context.getBean(SendEmail.class);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Integer> getSignConversionMap() {
		return (Map<String, Integer>) context
				.getBean(Beans.SIGN_CONVERSION_MAP);
	}

}
