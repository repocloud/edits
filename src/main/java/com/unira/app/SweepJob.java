package com.unira.app;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SweepJob {
	private static final Logger log = LogManager.getLogger(SweepJob.class);
	private final AppContext appContext;

	public SweepJob(AppContext appContext) {
		this.appContext = appContext;
	}

	public void run() {
		ExecutorService executorService = null;
		int timeout = appContext.getTimeoutForSweepProcess();
		try {
			String inputFolderPath = appContext.getInputFolder();

			// checking for submission files in input folder
			File inputFolder = new File(inputFolderPath);
			Collection<File> files = FileUtils.listFiles(inputFolder,
					new String[] { "zip" }, false);
			if (files.isEmpty()) {
				log.info("No submission files");
				return;
			}

			executorService = appContext.getExecutorService();

			//

			for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
				try {
					File archiveFile = iterator.next();
					FileProcessor fileProcessor = appContext.getFileProcessor();
					String archiveFileName = FilenameUtils.getName(archiveFile
							.getName());
					fileProcessor.setArchiveFileName(archiveFileName);
					executorService.submit(fileProcessor);
				} catch (Exception e) {
					log.error(e);
				}

			}
		} finally {
			if (null != executorService) {
				executorService.shutdown();
				try {
					executorService.awaitTermination(timeout, TimeUnit.HOURS);
					log.info("Shut down thread pool");
				} catch (InterruptedException e) {
					log.error(e);
				}
			}
		}
	}

}
