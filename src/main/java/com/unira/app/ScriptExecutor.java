package com.unira.app;

import java.io.File;
import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ScriptExecutor {
	private static final Logger log = LogManager
			.getLogger(ScriptExecutor.class);

	public boolean executeScript(String workingDirectory, String command,
			String... arguments) {
		try {
			// Command to be executed
			CommandLine commandLine = CommandLine.parse(command);
			// Adding arguments
			commandLine.addArguments(arguments);
			// Infinite timeout
			ExecuteWatchdog watchDog = new ExecuteWatchdog(
					ExecuteWatchdog.INFINITE_TIMEOUT);

			// Using Std out for the output/error stream
			PumpStreamHandler streamHandler = new PumpStreamHandler();

			// This is used to end the process when the JVM exits
			ShutdownHookProcessDestroyer processDestroyer = new ShutdownHookProcessDestroyer();

			// main command executor
			DefaultExecutor executor = new DefaultExecutor();

			// Setting the properties
			executor.setStreamHandler(streamHandler);
			executor.setWatchdog(watchDog);
			// Setting the working directory
			executor.setWorkingDirectory(new File(workingDirectory));

			executor.setProcessDestroyer(processDestroyer);
			executor.setExitValue(0);

			if (command.contains("update_tl")) {
				log.info("Command line argument:" + arguments[1]);
			}

			// Executing the command
			int exitValue = executor.execute(commandLine);

			return !executor.isFailure(exitValue);
		} catch (IllegalArgumentException iae) {
			log.info(iae);
		} catch (ExecuteException ee) {
			log.info(ee);
		} catch (IOException ioe) {
			log.info(ioe);
		}
		return false;
	}
}
