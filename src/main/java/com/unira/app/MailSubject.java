package com.unira.app;

public class MailSubject {

	String uniraFileUnzipMessage;
	String tlValidationErrorMessage;
	
	
	public String getUniraFileUnzipMessage() {
		return uniraFileUnzipMessage;
	}
	public void setUniraFileUnzipMessage(String uniraFileUnzipMessage) {
		this.uniraFileUnzipMessage = uniraFileUnzipMessage;
	}
	public String getTlValidationErrorMessage() {
		return tlValidationErrorMessage;
	}
	public void setTlValidationErrorMessage(String tlValidationErrorMessage) {
		this.tlValidationErrorMessage = tlValidationErrorMessage;
	}
	
	
}
