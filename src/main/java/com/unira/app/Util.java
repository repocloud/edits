package com.unira.app;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.unira.exception.SweepException;

public final class Util {
	
	private Util(){
		
	}
	
	/**
	 * 
	 * @param jobFilePath
	 * @return
	 */
	public synchronized static String readAndUpdateJobFile(String jobFilePath) {
		// get latest job id from job file
		String jobEntry = null;
		File jobfile = null;
		try {
			jobfile = new File(jobFilePath);
			jobEntry = FileUtils.readFileToString(jobfile, "UTF-8");

			// update job entry in job file
			int currentJobEntry = NumberUtils.toInt(jobEntry);
			int updatedJobId;
			String newJobId = "0000";
			if (currentJobEntry < 200) {
				updatedJobId = currentJobEntry + 1;
				newJobId = StringUtils.leftPad(Integer.toString(updatedJobId), 4, "0");
			}else{
				int resetJobId = 1;
				newJobId = StringUtils.leftPad(Integer.toString(resetJobId), 4, "0");
			}
			String currentJob = StringUtils.leftPad(Integer.toString(currentJobEntry), 4, "0");
			FileUtils.write(jobfile, newJobId, "UTF-8", false);
			
			return currentJob;
		} catch (IOException e) {
			SweepException exception = new SweepException(
					"Error getting latest JOB ID :", e);
			throw exception;
		}
	}
}
