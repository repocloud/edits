package com.unira.app;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unira.exception.SweepException;

/**
 * 
 * To do the file related operations in the sweep process
 * 
 */
public class FileProcessor implements Runnable {
	private static final Logger log = LogManager.getLogger(FileProcessor.class);
	private static final String JOB_CONSTANT = "UAVP";
	private String baseFolder;
	private String archiveFileName;
	private ScriptExecutor scriptExecutor;
	private Validator validator;
	private Map<String, Integer> signConversionMap;
	private String unzipScriptPath;
	private String extractScScriptPath;
	private String updateTlScriptPath;
	private String cleanUpScriptPath;
	private String inputFolder;
	private String unzipFolder;
	private String tlRecordFileName;
	private String scRecordFileName;
	private String jobFilePath;
	private boolean status = true;
	private String archiveFileBaseName = null;
	private String scriptCommand = null;
	private String updatedTlRecord;
	private String tlField;
	private String scRecordFilePath;
	
	public FileProcessor(){
		//JUnit test case implementation
	}
	
	public FileProcessor(ScriptExecutor scriptExecutor, Validator validator) {
		this.validator = validator;
		this.scriptExecutor = scriptExecutor;
	}

	public void setSignConversionMap(Map<String, Integer> signConversionMap) {
		this.signConversionMap = signConversionMap;
	}

	public void setBaseFolder(String baseFolder) {
		this.baseFolder = baseFolder;
	}

	public void setArchiveFileName(String archiveFileName) {
		this.archiveFileName = archiveFileName;
	}
	/**
	 * @param archiveFileBaseName the archiveFileBaseName to set
	 */
	public void setArchiveFileBaseName(String archiveFileBaseName) {
		this.archiveFileBaseName = archiveFileBaseName;
	}

	public void setUnzipScriptPath(String unzipScriptPath) {
		this.unzipScriptPath = unzipScriptPath;
	}

	public void setExtractScScriptPath(String extractScScriptPath) {
		this.extractScScriptPath = extractScScriptPath;
	}

	public void setUpdateTlScriptPath(String updateTlScriptPath) {
		this.updateTlScriptPath = updateTlScriptPath;
	}

	public void setCleanUpScriptPath(String cleanUpScriptPath) {
		this.cleanUpScriptPath = cleanUpScriptPath;
	}

	public void setInputFolder(String inputFolder) {
		this.inputFolder = inputFolder;
	}

	public void setUnzipFolder(String unzipFolder) {
		this.unzipFolder = unzipFolder;
	}

	public void setTlRecordFileName(String tlRecordFileName) {
		this.tlRecordFileName = tlRecordFileName;
	}

	public void setScRecordFileName(String scRecordFileName) {
		this.scRecordFileName = scRecordFileName;
	}

	public void setJobFilePath(String jobFilePath) {
		this.jobFilePath = jobFilePath;
	}
	/**
	 * @param scRecordFilePath the scRecordFilePath to set
	 */
	public void setScRecordFilePath(String scRecordFilePath) {
		this.scRecordFilePath = scRecordFilePath;
	}
	/**
	 * @param validator the validator to set
	 */
	public void setValidator(Validator validator) {
		this.validator = validator;
	}
	/**
	 * @param tlField the tlField to set
	 */
	public void setTlField(String tlField) {
		this.tlField = tlField;
	}
	
	/**
	 * Submission file and its related processing
	 */
	public void process() {
		try {
			String jobId = JOB_CONSTANT
					+ Util.readAndUpdateJobFile(jobFilePath);
			if (log.isInfoEnabled()) {
				log.info("job file path:  " + jobFilePath);
				log.info("Current JOB processing: " + jobId);
			}
			//

			// ---- Call shell script and do the below ----
			// Unzip the file to destination folder . Destination
			// folder name should e same as ZIP file . Extract the first line of
			// ZIP
			// file and create a file named 'TL' in the same folder .
			if (!unZipProcess())
				return;
				
			tlAccess();
						
			//validate TL 
			if (!tlValidation(tlField))
				return;
	
			// ---- Call shell script and do the below ----
			// Extract SC records from submission file and create new file named
			// SC.txt with it.
			if (!scExtraction())
				return;
				
			scRecordFilePath = unzipFolder + "/" + archiveFileBaseName
					+ "/" + scRecordFileName;

			createNewTL();
			
			// ---- Call shell script and do the below ----
			// Update the new TL record in the submission file and move the
			// submission file to output folder
			if (!writeNewTL())
				return;
			
			// send success mail

			//
		} finally {
			// delete the processing folder and move the archive to a backup
			// folder/
			cleanUpProcess();
		}

	}
	
	/**
	 *  New TL Creation
	 */
	private void createNewTL(){
		List<String> scRecords = readScRecordsFromFile(scRecordFilePath);

		int totalRecordCount = processRecords(scRecords);

		updatedTlRecord = updateTlRecord(tlField, totalRecordCount);

		if (log.isDebugEnabled()) {
			log.debug("updated TL record: ");
			log.debug(updatedTlRecord.length());
		}
	}
	
	/**
	 * TL Validation Process
	 * @param tlField
	 * @return
	 */
	private boolean tlValidation(String tlField){
		Status validationStatus = validator.validateTL(tlField);
		if (validationStatus != Status.SUCCESS) {
			if (log.isInfoEnabled()) {
				log.info("TL validation failed ");
			}
			// send email
			return false;

		}
		if (log.isInfoEnabled()) {
			log.info("TL validation success ");
		}
		return true;
	}
	
	/**
	 * SC Extraction Process
	 * @return
	 */
	private boolean scExtraction(){
		scriptCommand = "sh " + extractScScriptPath;
		status = scriptExecutor.executeScript(baseFolder, scriptCommand,
				archiveFileBaseName);
		if (!status) {
			log.error("SC record extraction failed");
			return false;
		}
		if (log.isInfoEnabled()) {
			log.info("SC record extraction success ");
		}
		return true;
	}
	
	/**
	 * Write New TL to Submission File
	 * @return
	 */
	private boolean writeNewTL(){
		scriptCommand = "sh " + updateTlScriptPath;
		status = scriptExecutor.executeScript(baseFolder, scriptCommand,
				archiveFileBaseName, updatedTlRecord);
		if (!status) {
			log.error("TL Update failed");
			return false;
		}

		if (log.isInfoEnabled()) {
			log.info("TL update success ");
		}
		return true;
		
	}
	
	/**
	 * Submission file Unzip process
	 * @return
	 */
	private boolean unZipProcess(){
		String archiveFilePath = inputFolder + "/" + archiveFileName;
		archiveFileBaseName = FilenameUtils.getBaseName(archiveFileName);
		scriptCommand = "sh " + unzipScriptPath;
		status = scriptExecutor.executeScript(baseFolder, scriptCommand,
				archiveFilePath, archiveFileBaseName);
		if (!status) {
			log.error("Unzip failed.");
			return false;
		}
		if (log.isInfoEnabled()) {
			log.info("Unzip success ");
		}
		return true;
	}
	
	/**
	 * Clean Up Process
	 */
	private void cleanUpProcess(){
		scriptCommand = "sh " + cleanUpScriptPath;
		status = scriptExecutor.executeScript(baseFolder, scriptCommand,
				archiveFileBaseName);
		if (!status) {
			log.error("Cleanup failed");
		}

		if (log.isInfoEnabled()) {
			log.info("Cleanup success ");
		}
	}
		
	/**
	 * TL Access process
	 */
	private void tlAccess(){
		String tlFilePath = unzipFolder + "/" + archiveFileBaseName + "/"
				+ tlRecordFileName;
		tlField = readTlFieldFromFile(tlFilePath);
		if (log.isInfoEnabled()) {
			log.debug("TL field:  " + tlField.length());
		}
	}
	
	/**
	 * Update new TL Detailed process
	 * @param tlField
	 * @param totalRecordCount
	 * @return
	 */
	private String updateTlRecord(String tlField, int totalRecordCount) {
		String totalRecordCountString = Integer.toString(totalRecordCount);
		totalRecordCountString = StringUtils.leftPad(totalRecordCountString, 9,
				"0");
		String beforeRecordCount = StringUtils.substring(tlField, 0, 34);
		String afterRecordCount = StringUtils.substring(tlField, 43);
		updatedTlRecord = beforeRecordCount + totalRecordCountString
				+ afterRecordCount;
		return updatedTlRecord;

	}

	/**
	 * Process SC Records
	 * @param scRecords
	 * @return
	 */
	private int processRecords(List<String> scRecords) {
		int totalNoOfScRecords = scRecords.size();
		log.info("Total number of SC records: " + totalNoOfScRecords);

		Iterator<String> scRecordIterator = scRecords.iterator();
		int totalRecordCount = 0;
		while (scRecordIterator.hasNext()) {
			String scRecord = scRecordIterator.next();
			String dollarAmount = getDollarAmountCount(scRecord, 14, 24);
			log.info("Dollar Amount: " + dollarAmount);
			String recordCount = getDollarAmountCount(scRecord, 24, 33);
			log.info("Record Count " + recordCount);
			totalRecordCount += NumberUtils.toInt(recordCount);
		}
		log.info("Total Record Count " + totalRecordCount);
		return totalRecordCount;
	}

	/**
	 * Extract SC Dollar Amount
	 * @param scRecord
	 * @return
	 */
	private String getDollarAmountCount(String scRecord, int StartLen, int EndLen) {
		String dollarValueFromScRecord = StringUtils
				.substring(scRecord, StartLen, EndLen);
		if (StringUtils.isNumeric(dollarValueFromScRecord)) {
			return dollarValueFromScRecord;
		}
		String lastChar = dollarValueFromScRecord
				.substring(dollarValueFromScRecord.length() - 1);
		if (lastChar.equals(Constants.LEFT_PARENTHESIS)) {
			dollarValueFromScRecord = StringUtils.replace(
					dollarValueFromScRecord, lastChar, "0");
			return dollarValueFromScRecord;
		}

		if (lastChar.equals(Constants.RIGHT_PARENTHESIS)) {
			dollarValueFromScRecord = StringUtils.replace(
					dollarValueFromScRecord, lastChar, "0");
			dollarValueFromScRecord = StringUtils.prependIfMissing(
					dollarValueFromScRecord, "-");
			return dollarValueFromScRecord;
		}

		int signValue = signConversionMap.get(lastChar);
		if (signValue < 0) {
			int absValue = Math.abs(signValue);
			dollarValueFromScRecord = StringUtils.replace(
					dollarValueFromScRecord, lastChar, Integer.toString(absValue));
			dollarValueFromScRecord = StringUtils.prependIfMissing(
					dollarValueFromScRecord, "-");
			return dollarValueFromScRecord;
		}
		dollarValueFromScRecord = StringUtils.replace(dollarValueFromScRecord,
				lastChar, Integer.toString(signValue));

		return dollarValueFromScRecord;
	}
	
	/**
	 * SC Record Read process
	 * @param scFilePaTH
	 * @return
	 */
	private List<String> readScRecordsFromFile(String scFilePaTH) {

		List<String> scRecords = null;
		try {
			scRecords = FileUtils.readLines(new File(scFilePaTH), "UTF-8");
		} catch (IOException e) {
			SweepException exception = new SweepException(
					"Error in reading SC records", e);
			throw exception;
		}
		return scRecords;
	}

	/**
	 * TL Record Read process
	 * @param tlFilePath
	 * @return
	 */
	private String readTlFieldFromFile(String tlFilePath) {
		try {
			tlField = FileUtils.readFileToString(new File(tlFilePath), "UTF-8");
		} catch (IOException e) {
			SweepException exception = new SweepException(
					"Error in reading TL field", e);
			throw exception;
		}
		return tlField;
	}

	public void run() {
		process();

	}

}
