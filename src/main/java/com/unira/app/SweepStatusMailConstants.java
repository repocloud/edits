package com.unira.app;

public final class SweepStatusMailConstants {
	public static final String UNZIPPED_FAIL_SUBJECT = "Sweep process unzip error.";
	public static final String UNZIP_FAIL_MESSAG_TEMPLATE = "Job: ${jobId} error. Submission file archive , ${submissionFilePath} unzip failed .";
	
	private SweepStatusMailConstants(){
		
	}
}
