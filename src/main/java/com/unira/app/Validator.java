package com.unira.app;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Validator {
	private static final Logger log = LogManager.getLogger(Validator.class);
	public static final String TL = "TL";
	public static final String BLANK = " ";
	private String tlSubmissionType;
	Status status;
	
	
	/**
	 * @param tlSubmissionType the tlSubmissionType to set
	 */
	public void setTlSubmissionType(String tlSubmissionType) {
		this.tlSubmissionType = tlSubmissionType;
	}

	/**
	 * 
	 * @param tlFilePath
	 * @return
	 */
	public Status validateTL(String tlField) {

		if (StringUtils.isBlank(tlField)) {
			return Status.INITIAL_TL_CHECK_FAILED;
		}
		status = doInitialTlValidation(tlField);
		if (status != Status.SUCCESS) {
			return status;
		} else {
			status = doSubsequentTlValidation(tlField);
			return status;
		}
	}
	
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status doInitialTlValidation(String tlField) {
		status = TLId(tlField);
		
		status = TLGroup(tlField);
		
		status = TLPlan(tlField);
		
		return status;
	}
	
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status TLId(String tlField){
		String tlId = StringUtils.substring(tlField, 0, 2);
		if (!StringUtils.equals(tlId, TL)) {
			if (log.isInfoEnabled()) {
				log.info("Id check failed:  " + tlId);
			}
			return Status.INITIAL_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}

	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status TLGroup(String tlField){
		String tlGroup = StringUtils.substring(tlField, 2, 6);
		if (!StringUtils.isNumeric(tlGroup)) {
			if (log.isInfoEnabled()) {
				log.info("TL groupcheck failed:  " + tlGroup);
			}
			return Status.INITIAL_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}
	
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status TLPlan(String tlField){
		String statPlan = StringUtils.substring(tlField, 6, 8);
		if (!StringUtils.containsAny(statPlan, "01", "02", "11", "41")) {
			if (log.isInfoEnabled()) {
				log.info("Stat plan check failed:  " + statPlan);
			}
			return Status.INITIAL_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status doSubsequentTlValidation(String tlField) {

		status = TLAcctDate(tlField);

		status = TLStatType(tlField);

		tlSubmissionType = StringUtils.substring(tlField, 13, 14);
		status = TLSubmissionType(tlSubmissionType);
		
		status = TLReSubmTypeCount(tlField);
		
		status = TLSubmissionCount(tlField);

		status = TLFinalParIndicator(tlField);
	
		status = TLSarMedia(tlField);
		return status;
	}
	
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status TLAcctDate(String tlField){
		String tlAcctMo = StringUtils.substring(tlField, 8, 10);
		if (!StringUtils.containsAny(tlAcctMo, "01", "02", "03", "04", "05",
				"06", "07", "08", "09", "10", "11", "12")) {
			if (log.isInfoEnabled()) {
				log.info("Account month check failed:  " + tlAcctMo);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}

		String tlAcctYr = StringUtils.substring(tlField, 10, 12);
		if (!StringUtils.isNumeric(tlAcctYr)) {
			if (log.isInfoEnabled()) {
				log.info("Account year check failed:  " + tlAcctYr);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}
	
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status TLStatType(String tlField){
		String tlTOS = StringUtils.substring(tlField, 12, 13);
		if (!StringUtils.containsAny(tlTOS, "1", "2", "3")) {
			if (log.isInfoEnabled()) {
				log.info("TOS check failed:  " + tlTOS);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}
	
	/**
	 * 
	 * @param tlSubmissionType
	 * @return
	 */
	private Status TLSubmissionType(String tlSubmissionType){
		if (!StringUtils.containsAny(tlSubmissionType, "1", "2", "3", "5", "6")) {
			if (log.isInfoEnabled()) {
				log.info("Tl submission type check failed:  "
						+ tlSubmissionType);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}
	
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status TLReSubmTypeCount(String tlField){
		String tlResubTyp = StringUtils.substring(tlField, 17, 18);
		String tlResubCnt = StringUtils.substring(tlField, 18, 20);
		if (log.isInfoEnabled()) {
			log.info("Tl Re submission type :  " + tlResubTyp);
			log.info("Tl Re submission count :  " + tlResubCnt);
		}
		if (StringUtils.containsAny(tlSubmissionType, "3", "5")) {
			TLSubmType3And5(tlResubTyp, tlResubCnt);
		} else {
			TLSubmTypeOth3And5(tlResubTyp, tlResubCnt);
		}
		return status;
		
	}
	
	/**
	 * 
	 * @param tlResubTyp
	 * @param tlResubCnt
	 * @return
	 */
	private Status TLSubmType3And5(String tlResubTyp, String tlResubCnt){
		if (!StringUtils.containsAny(tlResubTyp, "1", "2", "6")) {
			if (log.isInfoEnabled()) {
				log.info("Tl Re submission type check failed:  "
						+ tlResubTyp);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		if (!StringUtils.isNumeric(tlResubCnt)) {
			if (log.isInfoEnabled()) {
				log.info("Tl Re submission count check failed:  "
						+ tlResubCnt);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		if (StringUtils.equals(tlResubCnt, "00")) {
			if (log.isInfoEnabled()) {
				log.info("Tl Re submission count check failed:  "
						+ tlResubCnt);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}
	
	/**
	 * 
	 * @param tlResubTyp
	 * @param tlResubCnt
	 * @return
	 */
	private Status TLSubmTypeOth3And5(String tlResubTyp, String tlResubCnt){
		if (!StringUtils.equals(tlResubTyp, BLANK)) {
			if (log.isInfoEnabled()) {
				log.info("Tl Re submission type check failed:  "
						+ tlResubTyp);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		if (!StringUtils.equals(tlResubCnt, BLANK + BLANK)) {
			if (log.isInfoEnabled()) {
				log.info("Tl Re submission count check failed:  "
						+ tlResubCnt);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}
	
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status TLSubmissionCount(String tlField){
		String tlSubmissionCount = StringUtils.substring(tlField, 14, 16);
		if (!StringUtils.isNumeric(tlSubmissionCount)) {
			if (log.isInfoEnabled()) {
				log.info("Tl  submission count check failed:  "
						+ tlSubmissionCount);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		
		if (StringUtils.equals(tlSubmissionCount, "00")) {
			if (log.isInfoEnabled()) {
				log.info("Tl Submission count check failed:  "
						+ tlSubmissionCount);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}

	/**
	 * 
	 * @param tlFinPar
	 * @return
	 */
	private Status TLFinalParIndicator(String tlField){
		String tlFinPar = StringUtils.substring(tlField, 16, 17);
		if (StringUtils.equals(tlSubmissionType, "2") ){
			if (!StringUtils.containsAny(tlFinPar, BLANK, "X")) {
				if (log.isInfoEnabled()) {
					log.info("Tl  Fin Par check failed:  " + tlFinPar);
				}
				return Status.SUBSEQUENT_TL_CHECK_FAILED;
			}
		}else if (StringUtils.containsAny(tlFinPar, "X")){
			if (log.isInfoEnabled()) {
				log.info("Tl  Fin Par check failed:  " + tlFinPar);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}
		return Status.SUCCESS;
	}
	
	
	/**
	 * 
	 * @param tlField
	 * @return
	 */
	private Status TLSarMedia(String tlField){
		String tlSARmedia = StringUtils.substring(tlField, 32, 34);
		if (!StringUtils.containsAny(tlSARmedia, "R" + BLANK, "CC", "CN", "CP",
				"CZ")) {
			if (log.isInfoEnabled()) {
				log.info("Tl  SAR media check failed:  " + tlSARmedia);
			}
			return Status.SUBSEQUENT_TL_CHECK_FAILED;
		}

		return Status.SUCCESS;
	}
	
}
