package com.unira.app;

public enum Status {
	SUCCESS(0), INITIAL_TL_CHECK_FAILED(1), SUBSEQUENT_TL_CHECK_FAILED(2);
	private final int value;

	Status(final int newValue) {
		value = newValue;
	}

	public int getValue() {
		return value;
	}
}
