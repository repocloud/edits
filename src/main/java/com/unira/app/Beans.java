package com.unira.app;

/**
 * 
 * Spring beans
 * 
 */
public final class Beans {

	public static final String SWEEP_CONFIG = "sweepConfig";
	public static final String BASE_FOLDER = "baseBir";
	public static final String INPUT_FOLDER = "inputFolder";
	public static final String SCRIPTS_FOLDER = "scriptFolder";
	public static final String LOG_FOLDER = "logFolder";
	public static final String CONFIG_FOLDER = "configFolder";
	public static final String UNZIP_FOLDER = "unzipFolder";

	public static final String UNZIP_SCRIPT = "unzipScript";
	public static final String EXTRACT_SC_SCRIPT = "extractScScript";
	public static final String UPDATE_TL_SCRIPT = "updateTlScript";
	public static final String CLEANUP_SCRIPT = "cleanupScript";

	public static final String TL_EXTRACT_FILE_NAME = "tlExtractFileName";
	public static final String SC_EXTRACT_FILE_NAME = "scExtractFileName";
	public static final String EMAIL_LIST_FILE_NAME = "emailListFIleName";
	public static final String JOB_FILE_NAME = "JobFileName";
	public static final String JOB_FILE_PATH = "JobFilePath";

	public static final String EMAIL_ID_LIST = "emailIdList";
	public static final String SWEEP_JOB = "sweepJob";
	public static final String SEND_MAIL = "mail";
	public static final String MAIL_SUBJECT = "mailsubject";
	public static final String SIGN_CONVERSION_MAP = "signConversionMap";
	
	private Beans(){
		
	}
}