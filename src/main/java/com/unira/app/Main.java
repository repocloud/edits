package com.unira.app;

import javax.naming.ConfigurationException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.unira.config.AppConfig;

public class Main {
	private static final Logger log = LogManager.getLogger(Main.class);

	/**
	 * @param args
	 * @throws ConfigurationException
	 * 
	 */
	
	private Main(){
		
	}
	
	public static void main(String[] args) throws ConfigurationException {

		try {
			ApplicationContext appContext = new AnnotationConfigApplicationContext(
					AppConfig.class);
			SweepJob job = (SweepJob) appContext.getBean(Beans.SWEEP_JOB);
			job.run();

		} catch (Exception e) {
			log.error(e);
		}

	}

}
