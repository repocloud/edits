package com.unira.app;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;



public class SendEmail {

	List<String> emailList;
	String host;
	String from;
	String proto;

	public void setEmailList(List<String> emailList) {
		this.emailList = emailList;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setProto(String proto) {
		this.proto = proto;
	}

	public SendEmail() {

	}
		
	public static void main(String[] args) {

		new SendEmail().invokeMail("sandeep.pillai@nestgroup.net", "Hello Test");
	}

	public void invokeMail(String toAddress, String subject) {
		String to = toAddress;
		//String from = "sandeep.pillai@nestgroup.net";
		//String host = "casarray.nestgroup.net";
		Properties properties = System.getProperties();
		properties.setProperty(proto, host);
		Session session = Session.getDefaultInstance(properties);
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText("This is a Test Mail");
			Transport.send(message);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	public void triggerMailThread(final String toAddress, final String subject) {
		final SendEmail send = this;
		Thread mailThread = new Thread() {
			public void run() {
				try {
					send.invokeMail(toAddress, subject);
				} catch (Exception ex) {
					System.out.println("Error in processing mail for address ::" + toAddress);
				}
			}
		};
		mailThread.start();
	}

	public void triggerMailThreadFromMailList(final String subject) {
		final SendEmail send = this;
		for (final String mailid : emailList) {
			Thread mailThread = new Thread() {
				public void run() {
					try {
						send.invokeMail(mailid, subject);
					} catch (Exception ex) {
						System.out.println("Error in processing mail for address ::" + mailid);
					}
				}
			};
			mailThread.start();
		}
	}

}