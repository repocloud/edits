package com.unira.enums;

/**
 * 
 * @author Sandeep.pillai
 *
 */

public enum RecordIdentifier {

	TLRECORD("TL"), SCRECORD("SC");

	private RecordIdentifier(String value) {
		this.setValue(value);
	}

	private String value;

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}