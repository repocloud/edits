package com.unira.enums;

/**
 * 
 * @author Sandeep.pillai
 *
 */

public enum BusinessProcess {

	UNIRA("UNIRA");
	
	String value;
	
	private BusinessProcess(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}	
}