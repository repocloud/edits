package com.unira.exception;

public class SweepException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SweepException(String msg, Throwable throwable) {
		super(msg, throwable);
	}

	public SweepException(String msg) {
		super(msg);
	}

}
