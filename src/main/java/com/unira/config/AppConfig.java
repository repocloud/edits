package com.unira.config;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;

import com.unira.app.AppContext;
import com.unira.app.Beans;
import com.unira.app.Constants;
import com.unira.app.FileProcessor;
import com.unira.app.MailSubject;
import com.unira.app.ScriptExecutor;
import com.unira.app.SendEmail;
import com.unira.app.SweepJob;
import com.unira.app.Validator;
import com.unira.exception.SweepException;

@Configuration
@PropertySource("classpath:unira.properties")
public class AppConfig {
	private static final Logger log = LogManager.getLogger(AppConfig.class);

	@Autowired
    private Environment environment;
	
	@Bean
	public AppContext getAppContext() {
		return new AppContext();
	}

	@Bean(name = Beans.SWEEP_CONFIG)
	public org.apache.commons.configuration.Configuration getSweepConfig() {
		org.apache.commons.configuration.Configuration sweepConfiguration;
		try {
			sweepConfiguration = new PropertiesConfiguration(
					Constants.CONFIG_FILE_PATH);
		} catch (ConfigurationException e) {
			SweepException sweepException = new SweepException(
					"Error reading Config: ", e);
			throw sweepException;
		}
		return sweepConfiguration;
	}

	@Bean(name = Beans.BASE_FOLDER)
	public String getBasefolder() {
		String baseFolder = getSweepConfig()
				.getString(Constants.BASE_DIRECTORY);
		return baseFolder;
	}

	@Bean(name = Beans.TL_EXTRACT_FILE_NAME)
	public String getTlExtractFileName() {
		return Constants.TL_EXTRACT_FILENAME;
	}

	@Bean(name = Beans.SC_EXTRACT_FILE_NAME)
	public String getScExtractFileName() {
		return Constants.SC_EXTRACT_FILENAME;
	}

	@Bean(name = Beans.EMAIL_LIST_FILE_NAME)
	public String getEmailListFileName() {
		return Constants.EMAIL_LIST_FILE_NAME;
	}

	@Bean(name = Beans.JOB_FILE_NAME)
	public String getJobFileName() {
		return Constants.JOB_FILE_NAME;
	}

	@Bean(name = Beans.INPUT_FOLDER)
	public String getInputFolder() {
		String inputFolder = getBasefolder() + "/Input";
		return inputFolder;
	}

	@Bean(name = Beans.SCRIPTS_FOLDER)
	public String getScriptsFolder() {
		String scriptsFolder = getBasefolder() + "/scripts";
		return scriptsFolder;
	}

	@Bean(name = Beans.CONFIG_FOLDER)
	public String getConfigFolder() {
		String configFolder = getBasefolder() + "/config";
		return configFolder;
	}

	@Bean(name = Beans.UNZIP_FOLDER)
	public String getUnzipFolder() {
		String unzipFolder = getInputFolder() + "/Unzipped";
		return unzipFolder;
	}

	@Bean(name = Beans.UNZIP_SCRIPT)
	public String getUnzipScriptPath() {
		String unzipScriptPath = getScriptsFolder() + "/unzip.sh";
		return unzipScriptPath;
	}

	@Bean(name = Beans.EXTRACT_SC_SCRIPT)
	public String getExtractScScriptPathy() {
		String extractScScriptPath = getScriptsFolder() + "/extract_sc.sh";
		return extractScScriptPath;
	}

	@Bean(name = Beans.UPDATE_TL_SCRIPT)
	public String getUpdateTlScriptPath() {
		String updateTlScriptPath = getScriptsFolder() + "/update_tl.sh";
		return updateTlScriptPath;
	}

	@Bean(name = Beans.CLEANUP_SCRIPT)
	public String getCleanUpScriptPath() {
		String cleanUpScriptPath = getScriptsFolder() + "/cleanup.sh";
		return cleanUpScriptPath;
	}

	@Bean(name = Beans.LOG_FOLDER)
	public String getBaseDirectory() {
		String logFolder = getBasefolder() + "/logs";
		return logFolder;
	}

	@Bean(name = Beans.JOB_FILE_PATH)
	public String getJobFilePath() {
		String jobFilePath = getConfigFolder() + "/" + getJobFileName();
		return jobFilePath;
	}

	@Bean(name = Beans.SWEEP_JOB)
	public SweepJob getSweepJob() {
		SweepJob sweepJob = new SweepJob(getAppContext());
		return sweepJob;
	}
	
	@Bean(name = Beans.SEND_MAIL)
	public SendEmail getMail() {
		SendEmail mail = new SendEmail();
		mail.setEmailList(getEmailIdList());
		mail.setFrom(environment.getProperty("unira.mail.from"));
		mail.setHost(environment.getProperty("unira.mail.host"));
		mail.setProto(environment.getProperty("unira.mail.proto"));
		return mail;
	}
	
	@Bean(name = Beans.MAIL_SUBJECT)
	public MailSubject getMailSubject() {
		MailSubject mailSubject = new MailSubject();
		mailSubject.setUniraFileUnzipMessage(environment.getProperty("unira.sweep.mail.fileunzip.failure"));
		mailSubject.setTlValidationErrorMessage(environment.getProperty("unira.sweep.mail.tlvalidation.failure"));
		return mailSubject;
	}

	@Bean
	public ExecutorService getExecutorService() {
		ExecutorService executorService;
		boolean isParallelProcessing = getSweepConfig().getBoolean(
				Constants.IS_PARALLEL_SWEEP);
		int threadPoolSize = getSweepConfig()
				.getInt(Constants.THREAD_POOL_SIZE);
		if (isParallelProcessing) {
			executorService = Executors.newFixedThreadPool(threadPoolSize);
			if (log.isInfoEnabled()) {
				log.info("Parallel processing");
				log.info("Threadpool initialized with size: " + threadPoolSize);
			}
		} else {
			if (log.isInfoEnabled()) {
				log.info("Sequential processing");
			}
			executorService = Executors.newSingleThreadExecutor();
		}
		return executorService;
	}

	@Bean
	@Scope("prototype")
	public FileProcessor getFileProcessor() {
		FileProcessor fileProcessor = new FileProcessor(getScriptExecutor(),
				getvValidator());
		fileProcessor.setBaseFolder(getBasefolder());
		fileProcessor.setExtractScScriptPath(getExtractScScriptPathy());
		fileProcessor.setInputFolder(getInputFolder());
		fileProcessor.setScRecordFileName(getScExtractFileName());
		fileProcessor.setSignConversionMap(getSignConversionMap());
		fileProcessor.setTlRecordFileName(getTlExtractFileName());
		fileProcessor.setUnzipFolder(getUnzipFolder());
		fileProcessor.setUnzipScriptPath(getUnzipScriptPath());
		fileProcessor.setUpdateTlScriptPath(getUpdateTlScriptPath());
		fileProcessor.setJobFilePath(getJobFilePath());
		fileProcessor.setCleanUpScriptPath(getCleanUpScriptPath());
		return fileProcessor;
	}

	@Bean
	public ScriptExecutor getScriptExecutor() {
		return new ScriptExecutor();
	}

	@Bean
	public Validator getvValidator() {
		return new Validator();
	}

	@Bean(name = Beans.SIGN_CONVERSION_MAP)
	public Map<String, Integer> getSignConversionMap() {
		Map<String, Integer> signConversionMap = new HashMap<String, Integer>();
		signConversionMap.put("A", 1);
		signConversionMap.put("B", 2);
		signConversionMap.put("C", 3);
		signConversionMap.put("D", 4);
		signConversionMap.put("E", 5);
		signConversionMap.put("F", 6);
		signConversionMap.put("G", 7);
		signConversionMap.put("H", 8);
		signConversionMap.put("I", 9);
		signConversionMap.put("J", -1);
		signConversionMap.put("K", -2);
		signConversionMap.put("L", -3);
		signConversionMap.put("M", -4);
		signConversionMap.put("N", -5);
		signConversionMap.put("O", -6);
		signConversionMap.put("P", -7);
		signConversionMap.put("Q", -8);
		signConversionMap.put("R", -9);
		return signConversionMap;
	}

	@Bean(name = Beans.EMAIL_ID_LIST)
	public List<String> getEmailIdList() {
		String emailListFilePath = getConfigFolder() + "/"
				+ getEmailListFileName();
		List<String> emailList = null;
		try {
			emailList = FileUtils.readLines(new File(emailListFilePath),
					"UTF-8");
		} catch (IOException e) {
			log.error("Error in reading email list ", e);
		}
		return emailList;
	}
}
